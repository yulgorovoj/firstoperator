#include <iostream>
using namespace std;


void OoE(int par)
{
    const int N = 10;
    for (int i = 0; i < N; ++i)
        if (i % 2 == par % 2)
            cout << i << endl;
}

int main()
{
    int even;
    cin >> even;
    OoE(even);
    return 0;
}